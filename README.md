# Reproduce artifact upload failure

This project is meant as a reproducer of https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26868.

The basic idea is to show that if `gitlab-runner-helper artifacts-uploader` crashes, the job will
succeed despite the artifacts not being uploaded.

In our case, we are seeing the runner crash when `Do` ("do http request") is invoked:
https://gitlab.com/gitlab-org/gitlab-runner/-/blob/e3a736ef46de5bb0797c2232d8ce1845119dfb6d/network/ratelimit_requester.go#L72
   - This reproducer does not help explain *why* `Do` crashes, but it does show that the `gitlab-runner` is not
     reacting appropriately when it crashes

## Setup

1. Create a project with the `.gitlab-ci.yml` provided here:
   - The script generates a 1GB artifact -- the idea is for the upload to take long enough for us to be able to kill
     the executable in the middle of the upload
2. Install and register a `gitlab-runner` to the project
   - In my case, I used a Raspberry Pi running Ubuntu
   - tags: `my-rpi`
   - executor: `docker`
   - default image: `docker:latest`
3. Run CI once to make sure the runner's host fetches the appropriate `gitlab-runner-helper`
   - This job should succeed!
4. Copy the `Dockerfile` and `gitlab-runner-helper` script provided by this project to the runner's host
   ```shell
   scp Dockerfile gitlab-runner-helper pi@my-rpis-ip:~
   ```
5. Connect to the runner's host and use the provided scripts generate a `gitlab-runner-helper` based on the original:
   - Note: In my case, the relevant tag was `arm64-738bbe5a`, but this may be different if you are using a different
     runner version or host machine
   1. Retag the original gitlab-runner-helper:
   ```shell
   $ ssh pi@my-rpis-ip
   pi$ docker images
   REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
   ...
   gitlab/gitlab-runner-helper   arm64-738bbe5a      622745ed9243        ***                 75.6MB
   pi$ docker tag gitlab/gitlab-runner-helper:arm64-738bbe5a gitlab/gitlab-runner-helper:orig
   pi$ docker images
   REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
   ...
   gitlab/gitlab-runner-helper   arm64-738bbe5a      622745ed9243        ***                 75.6MB
   gitlab/gitlab-runner-helper   orig                622745ed9243        ***                 75.6MB
   ``` 
   2. Build a `gitlab-runner-helper` based on the old one:
   ```shell
   pi$ ls
   Dockerfile gitlab-runner-helper
   pi$ docker build -t gitlab/gitlab-runner-helper:arm64-738bbe5a .
   pi$ docker images
   REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
   ...
   gitlab/gitlab-runner-helper   arm64-738bbe5a      8bf0a0468dcf        ***                 75.6MB
   gitlab/gitlab-runner-helper   orig                622745ed9243        ***                 75.6MB
   ```
6. Run CI again, the "new" `gitlab-runner-helper` image will be used automatically:
   ```
   Uploading artifacts...
   large_file: found 1 matching files and directories
   Job succeeded
   ```
   - But the 1GB artifact has not been uploaded!
