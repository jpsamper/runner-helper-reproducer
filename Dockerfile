FROM gitlab/gitlab-runner-helper:orig

RUN mv /usr/bin/gitlab-runner-helper /usr/local/bin/gitlab-runner-helper
COPY gitlab-runner-helper /usr/bin/gitlab-runner-helper
RUN chmod +x /usr/bin/gitlab-runner-helper
